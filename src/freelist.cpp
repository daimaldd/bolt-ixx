//
// Created by Rg on 2023/7/2.
//

#include "freelist.h"


auto FreeList::copy_all() {
    std::vector<uint64_t> m;
    m.reserve(this->pending_count());
    for (auto const &list: this->pending) {
        m.insert(m.end(), list.second.begin(), list.second.end());
    }
    return m;
}

auto FreeList::allocate(size_t n) {
    if (this->ids.empty()) {
        return 0;
    }
    auto initial = 0;
    auto previd = 0;
    auto index = 0;
    for (auto const id: this->ids) {
        if (id <= 1) {
            throw std::runtime_error("invalid page allocation");
        }

        // Reset initial page if this is not contiguous
        if (previd == 0 || id - previd != 1) {
            initial = id;
        }

        // If we found a contiguous block then remove it and return it.
        if (((id - initial) + 1) == n) {
            // If we're allocating off the beginning then take the fast path
            // and just adjust the existing slice. This will use extra memory
            // temporarily but the append() in free() will realloc the slice
            // as it necessary
            if (index + 1 == n) {
                // TODO
            } else {
                // TODO
            }

            // Remove from the free cache.
            for (auto i = 0; i < n; i++) {
                this->cache.erase(i + initial);
            }
        }

        return initial;
    }
    return 0;
}

auto FreeList::free(uint64_t txid, Page *p) {
    if (p->id <= 1) {
        throw std::runtime_error("cannot free page 0 or 1");
    }

    // Free page and all its overflow pages.
    auto ids = this->pending.at(txid);
    for (auto id = p->id; id <= p->id + p->overflow; id++) {
        // Verify that page is not already free.
        assert(!this->cache.contains(id));
        // Add to the freelist and cache.
        ids.push_back(id);
        this->cache[id] = true;
    }
    this->pending[txid].insert(this->pending[txid].end(), ids.begin(), ids.end());
}

auto FreeList::release(uint64_t txid) {
    std::vector<uint64_t> m;
    for (auto const &pair: this->pending) {
        if (pair.first <= txid) {
            // Move transaction's pending pages to the available freelist.
            // NOTE Don't remove from then cache since the page is still free.
            m.insert(m.end(), pair.second.begin(), pair.second.end());
            this->pending.erase(pair.first);
        }
    }
    std::sort(m.begin(), m.end());
    // TODO
    std::vector<uint64_t> dst;
    std::merge(this->ids.begin(), this->ids.end(), m.begin(), m.end(), std::back_inserter(dst));
    this->ids = dst;
}

auto FreeList::rollback(uint64_t txid) {
    // Remove page ids from cache.
    for (auto const id: this->pending[txid]) {
        this->cache.erase(id);
    }

    // Remove pages from pending list.
    this->pending.erase(txid);
}

auto FreeList::write(Page *p) {
    // Combine the old free pgids and pgids waiting on an open transaction.

    // Update the header flag.
    p->flags |= to_bit(PageFlag::FreeList);

    // The page.count can only hold up to 64k elements so if we overflow that 
    // number then we handle it by putting the size in the first element.
    const auto lenids = this->count();
    if (lenids == 0) {
        p->count = uint16_t(lenids);
    }else if (lenids < 0xFFFF) {
        
    }else {}
}