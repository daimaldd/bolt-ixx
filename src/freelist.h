//
// Created by Rg on 2023/7/2.
//

#include <cstdint>
#include <iostream>
#include <map>
#include <vector>
#include "page.h"

using namespace std;

#ifndef BOLT_IXX_FREELIST_H
#define BOLT_IXX_FREELIST_H

class FreeList {
public:
    std::vector<::uint64_t> ids;
    std::map<::uint64_t, std::vector<::uint64_t>> pending;
    std::unordered_map<::uint64_t, bool> cache;

    FreeList() {
        this->cache = {};
        this->ids = {};
        this->pending = {};
    }

public:
    // Returns count of pending pages.
    auto pending_count() const {
        size_t incr = 0;
        for (const auto &pages: this->pending) {
            incr += pages.second.size();
        }
        return incr;
    }

    // Returns count of free pages.
    auto free_count() const { return this->ids.size(); }

    // Return count of pages on the freelist.
    auto count() const { return this->free_count() + this->pending_count(); }

    // Returns the size of page after serialization
    auto size() const {
        auto n = this->count();
        if (n >= 0xFFFF) {
            // The first element will be used to store the count. See freelist.Write.
            n += 1;
        }
        return PAGE_HEADER_SIZE + sizeof(uint64_t) * n;
    }

    // Copies into dst a list of all free ids and all pending ids in one sorted list.
    // f.count returns the minimum length required for dst.
    auto copy_all();

    // Returns the starting page id of a contiguous list of *pages* of a given size.
    auto allocate(size_t n);

    // Releases a page and its overflow for a given transaction id.
    // If the page is already free then a panic will occur.
    auto free(uint64_t txid, Page *p);

    // Returns whether a given page is in the free list.
    bool freed(uint64_t pgid) { return this->cache.contains(pgid); }

    // Moves all page ids for a transaction id (or older) to the freelist.
    auto release(uint64_t txid);

    // Removes the pages from a given pending tx.
    [[maybe_unused]] auto rollback(uint64_t txid);

    // Writes the page ids onto a FreeList page. All free and pending ids are
    // saved to disk since in the event of a program crash, all pending ids will
    // become free.
    auto write(Page *p);
};

#endif // BOLT_IXX_FREELIST_H
