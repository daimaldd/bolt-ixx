//
// Created by Rg on 2023/7/2.
//

#include "page.h"
#include "db.h"

using namespace std;

auto Page::typ() {
    if ((this->flags & to_bit(PageFlag::Branch)) != 0) {
        return "branch";
    } else if ((this->flags & to_bit(PageFlag::Leaf)) != 0) {
        return "leaf";
    } else if ((this->flags & to_bit(PageFlag::FreeList)) != 0) {
        return "freelist";
    } else if ((this->flags & to_bit(PageFlag::Meta)) != 0) {
        return "meta";
    } else {
        return "unknown";
    }
}

auto Page::meta() {
    return static_cast<class Meta *>(this->ptr);
}


auto Page::leaf_page_elements() {
    if (this->count == 0) {
        return (LeafPageElement *)nullptr;
    }
    auto ptr = reinterpret_cast<LeafPageElement *>(this->ptr);
    return ptr;
}

auto Page::leaf_page_element(uint16_t index) {
    auto elements = this->leaf_page_elements();
    if (elements == nullptr) {
        return elements;
    }

    return &elements[index];
}

auto Page::branch_page_elements() {
    if (this->count == 0) {
        return (BranchPageElement *)nullptr;
    }
    auto ptr = reinterpret_cast<BranchPageElement *>(this->ptr);
    return ptr;
}

auto Page::branch_page_element(uint16_t index) {
    auto elements = this->branch_page_elements();
    if (elements == nullptr) {
        return elements;
    }

    return &elements[index];
}