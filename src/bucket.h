//
// Created by Rg on 2023/7/2.
//

#include "cstdint"
#include "page.h"

#ifndef BOLT_IXX_BUCKET_H
#define BOLT_IXX_BUCKET_H


struct _Bucket {
    uint64_t root;
    uint64_t sequence;
};

class Bucket {
    _Bucket * sub_bucket;
public:
    // subbucket cache
    Page *page;
    // inline page reference
};

#endif //BOLT_IXX_BUCKET_H
