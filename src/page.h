//
// Created by Rg on 2023/7/2.
//
#include "cstdint"
#include <atomic>
#include <iostream>
#include <cstdlib>
#include "types.h"

#ifndef BOLT_IXX_PAGE_H
#define BOLT_IXX_PAGE_H

class BranchPageElement {
    uint32_t pos;
    uint32_t ksize;
    uint64_t pgid;

    auto key() const {
        auto ptr = reinterpret_cast<const char *>(this);
        return std::basic_string(&ptr[pos], &ptr[pos + ksize]);
    }

};

class LeafPageElement {
    uint32_t flags;
    uint32_t pos;
    uint32_t ksize;
    uint32_t vsize;

    auto key() const {
        auto ptr = reinterpret_cast<const char *>(this);
        return std::basic_string(&ptr[pos], &ptr[pos + ksize]);
    }

    auto value() const {
        auto ptr = reinterpret_cast<const char *>(this);
        auto slice = ByteSlice(ptr + this->pos, this->vsize);
        return slice;
    }
};

class Page {
public:
    int64_t id;
    int16_t flags;
    int16_t count;
    int32_t overflow;
    void *ptr;

    auto typ();

    auto meta();

    auto leaf_page_element(uint16_t index);

    auto leaf_page_elements();

    // Retrieves the branch node by index
    auto branch_page_element(uint16_t index);

    // Retrieves a lis of branch nodes.
    auto branch_page_elements();

    // dump writes b bytes of the page to STDERR as hex output.
    auto hexdump(int n) {
        const void *_ptr = static_cast<const void *>(this);
        const auto *bytes = static_cast<const unsigned char *>(_ptr);
        for (int i = 0; i < n; i++) {
            std::cout << static_cast<int>(bytes[i]) << std::endl;
        }
    }
};


const auto PAGE_HEADER_SIZE = sizeof(Page);
const auto MIN_KEYS_PER_PAGE = 2;
const auto BRANCH_PAGE_ELEMENT_SIZE = sizeof(BranchPageElement);
const auto LEAF_PAGE_ELEMENT_SIZE = sizeof(LeafPageElement);

enum class PageFlag : uint16_t {
    Branch = 0x01,
    Leaf = 0x02,
    Meta = 0x04,
    FreeList = 0x10,
};

static inline uint16_t to_bit(PageFlag flag) {
    return static_cast<uint16_t >(flag);
}

const auto BUCKET_LEAF_FLAG = 0x01;

// Represents human readable information about a page.
class PageInfo {
public:
    int id;

};

#endif //BOLT_IXX_PAGE_H
