//
// Created by Rg on 2023/7/2.
//
#include "cstdint"

#ifndef BOLT_IXX_DB_H
#define BOLT_IXX_DB_H


class DB {

};

class Meta {
public:
    uint32_t magic;
    uint32_t version;
    uint32_t page_size;
    uint32_t flags;
};


#endif //BOLT_IXX_DB_H
