#include "src/freelist.h"
#include "src/page.h"
#include <cstdlib>
#include <iostream>

auto foo() { return 100; }

int main() {
    std::cout << "Hello, World!" << PAGE_HEADER_SIZE << std::endl;
    auto n = foo();

    size_t pageSize = 4096; // 假设一页内存的大小为 4096 字节

    // 使用 std::aligned_alloc 分配一页内存
    void *memory = std::aligned_alloc(pageSize, pageSize);
    auto page = reinterpret_cast<Page *>(memory);
    page->count = 65;
    page->flags = 66;

    auto *bytePtr = reinterpret_cast<unsigned char *>(memory);

    // 使用字节指针访问内存中的字节
    for (size_t i = 0; i < pageSize; ++i) {
        std::cout << static_cast<::uint8_t>(bytePtr[i]) << " ";
    }
    std::cout << std::endl;
    std::free(memory);
    auto flist = FreeList();
    std::cout << "What" << std::endl;

    std::vector<int> a = {1, 3, 4, 5, 6, 6, 90, 1};
    std::vector<int> b;
    b.insert(b.end(), a.begin(), a.end());
    for (auto el: b) {
        std::cout << el << std::endl;
    }
    return 0;
}
