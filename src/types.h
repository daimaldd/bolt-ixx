//
// Created by Rg on 2023/7/2.
//

#include <iostream>

#ifndef BOLT_IXX_TYPES_H
#define BOLT_IXX_TYPES_H

using namespace std;

class ByteSlice {
    int _len;
    const char *_ptr;
public:
    ByteSlice(const char *ptr, int len) {
        this->_ptr = ptr;
        this->_len = len;
    }

    auto to_slice() const {
        auto ptr = reinterpret_cast<const char *>(this->_ptr);
        return std::basic_string(&ptr[0], &ptr[this->_len]);
    }
};


#endif //BOLT_IXX_TYPES_H
